from datetime import datetime
import pkgutil

__path__ = pkgutil.extend_path(__path__, __name__)

__title__ = 'woob'
__version__ = '3.4'
__author__ = 'Romain Bignon'
__copyright__ = 'Copyright(C) 2010-%s Romain Bignon' % datetime.today().year
